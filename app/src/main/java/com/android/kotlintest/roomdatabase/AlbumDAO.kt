package com.android.kotlintest.roomdatabase

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.android.kotlintest.models.Albums

@Dao
interface AlbumDAO {

    @Insert
    fun saveAlbums(albums: Albums)

    @Query(value = "Select * from Albums")
    fun getAllAlbums() : List<Albums>

    @Query("DELETE FROM Albums")
    fun deleteAll()


}