package com.android.kotlintest.roomdatabase

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.kotlintest.models.Albums

@Database (entities = [(Albums::class)],version = 1)
abstract class AppDb : RoomDatabase() {

    abstract fun albumDao(): AlbumDAO
}