package com.android.kotlintest.activity

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.android.kotlintest.R
import com.android.kotlintest.base.BaseActivity
import com.android.kotlintest.base.getViewModel
import com.android.kotlintest.models.ApiResponse
import com.android.kotlintest.network.STATUS
import com.android.kotlintest.ui.repository.AlbumsRepository
import com.android.kotlintest.viewmodels.AlbumsListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.android.kotlintest.adapter.AlbumsAdapter
import com.android.kotlintest.models.Albums
import com.android.kotlintest.roomdatabase.AppDb


class AlbumsActivity : BaseActivity() , CoroutineScope {

    private lateinit var linearLayoutManager: LinearLayoutManager
    private val albumsViewModel : AlbumsListViewModel by lazy {
        getViewModel({ AlbumsListViewModel(AlbumsRepository.getInstance()) })
    }

    fun initializeRecylerView(){
        linearLayoutManager = LinearLayoutManager(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initializeRecylerView()

        albumsViewModel.observeAlbumsLiveData().observe(this, Observer<ApiResponse> { apiResponse: ApiResponse? ->
            when(apiResponse?.status){
                STATUS.LOADING -> {
                    progress_circular.visibility = View.VISIBLE
                    }
                STATUS.SUCCESS -> {
                    progress_circular.visibility = View.GONE
                    var albums = apiResponse.result as? List<Albums>
                    albums?.apply {
                        var albumsAdapter = AlbumsAdapter(this@AlbumsActivity,albums)
                        recyclerView.layoutManager = linearLayoutManager
                        recyclerView.adapter = albumsAdapter
                    }
                }
                STATUS.ERROR -> {
                    progress_circular.visibility = View.GONE
                    Toast.makeText(this,apiResponse.error,Toast.LENGTH_SHORT).show()
                }
            }
        })

        CoroutineScope(coroutineContext).launch {
            var db = Room.databaseBuilder(applicationContext,AppDb::class.java,"AlbumDB").build()
            albumsViewModel.getAlbumsList(db)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        coroutineContext.cancel();
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home ->{
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main


}
