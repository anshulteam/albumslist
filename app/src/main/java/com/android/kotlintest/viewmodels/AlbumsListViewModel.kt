package com.android.kotlintest.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.kotlintest.KotlinTest
import com.android.kotlintest.models.Albums
import com.android.kotlintest.models.ApiResponse
import com.android.kotlintest.network.STATUS
import com.android.kotlintest.roomdatabase.AppDb
import com.android.kotlintest.ui.repository.AlbumsRepository
import com.android.kotlintest.utils.Utils
import kotlinx.coroutines.*

class AlbumsListViewModel(private val albumsRepository: AlbumsRepository) : ViewModel(){

    private val albumsLiveData : MutableLiveData<ApiResponse> = MutableLiveData()
    fun observeAlbumsLiveData() : MutableLiveData<ApiResponse>{
        return albumsLiveData
    }

    suspend fun getAlbumsList(appDb : AppDb) {
            viewModelScope.launch(Dispatchers.IO) {
                albumsLiveData.postValue(ApiResponse(STATUS.LOADING,null,null))
                coroutineScope {
                    try{
                        var albumsList = async  {
                            albumsRepository.getAlbumsList(appDb)
                        }.await()
                        val sortedAlbumsList : List<Albums> = albumsList.sortedBy { it.title }
                        if(Utils.isNetworkAvailable(KotlinTest.instance)){
                            appDb.albumDao().deleteAll()
                            sortedAlbumsList.forEach {
                                var album = Albums()
                                album.id = it.id
                                album.title = it.title
                                appDb.albumDao().saveAlbums(album)
                            }
                        }
                        albumsLiveData.postValue(ApiResponse(STATUS.SUCCESS,sortedAlbumsList,null))
                    } catch (e :java.lang.Exception){
                        albumsLiveData.postValue(ApiResponse(STATUS.ERROR,null,e.localizedMessage))
                    }
                }

            }
    }

}