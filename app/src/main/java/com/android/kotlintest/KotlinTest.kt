package com.android.kotlintest

import android.app.Application

class KotlinTest : Application(){

    companion object{
        var instance : Application?= null
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}