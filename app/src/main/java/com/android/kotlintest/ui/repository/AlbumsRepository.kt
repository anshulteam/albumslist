package com.android.kotlintest.ui.repository

import com.android.kotlintest.KotlinTest
import com.android.kotlintest.models.Albums
import com.android.kotlintest.network.RetroFitApiService
import com.android.kotlintest.roomdatabase.AppDb
import com.android.kotlintest.utils.Utils

class AlbumsRepository {

    companion object{
        private var mInstance : AlbumsRepository?=null
        fun getInstance() = mInstance?: AlbumsRepository().also { mInstance = it }
        }


    suspend fun getAlbumsList(appDb : AppDb) : List<Albums> {
        if(Utils.isNetworkAvailable(KotlinTest.instance)){
            return RetroFitApiService.create().getAlbumsList()
        }
        return appDb.albumDao().getAllAlbums()
    }
}