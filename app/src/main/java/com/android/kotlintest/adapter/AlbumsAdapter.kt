package com.android.kotlintest.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.kotlintest.R
import com.android.kotlintest.models.Albums
import kotlinx.android.synthetic.main.album_list_item.view.*


class AlbumsAdapter(
    private var paramContext: Context,
    private var listOfAlbums: List<Albums>?
) :
    RecyclerView.Adapter<AlbumsAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var albumName = view.album_name


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(paramContext).inflate(
                R.layout.album_list_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var albums = listOfAlbums?.get(position)
        holder.albumName.text = albums?.title.toString()

    }

    override fun getItemCount(): Int {
        return listOfAlbums!!.size
    }


}