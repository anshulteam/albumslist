package com.android.kotlintest.network

import com.android.kotlintest.models.Albums

interface RetroFitApiInterface {

    @retrofit2.http.GET(NetworkConstant.ALBUMS)
    suspend  fun getAlbumsList(): List<Albums>
}