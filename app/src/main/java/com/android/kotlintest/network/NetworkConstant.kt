package com.android.kotlintest.network

class NetworkConstant {

    companion object{
        const val ALBUMS  = "albums";
    }
}

enum class STATUS{
    SUCCESS,
    LOADING,
    ERROR
}