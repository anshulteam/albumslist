package com.android.kotlintest.network

import com.android.kotlintest.BuildConfig


import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit


class RetroFitApiService {
    companion object Factory {

        fun create(): RetroFitApiInterface {

            val retrofit = retrofit2.Retrofit.Builder()
                .addConverterFactory(retrofit2.converter.gson.GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .client(httpClient)
                .build()
            return retrofit.create(RetroFitApiInterface::class.java)

        }


        private val httpClient: OkHttpClient
            private get() {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val httpClient = OkHttpClient.Builder()
                httpClient.addInterceptor { chain ->
                    val original = chain.request()
                    val request = original.newBuilder()
                        .method(original.method, original.body)
                        .build()
                    chain.proceed(request)
                }.connectTimeout(100, TimeUnit.SECONDS)
                    .writeTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(300, TimeUnit.SECONDS)
                return httpClient.addInterceptor(interceptor).build()
            }


    }
}