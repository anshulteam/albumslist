package com.android.kotlintest.models

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Albums {

    @PrimaryKey
    var id: Int =0

    @ColumnInfo (name ="title")
    var title : String?= null
}