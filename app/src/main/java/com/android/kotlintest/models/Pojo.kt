package com.android.kotlintest.models


import com.android.kotlintest.network.STATUS as Status

data class ApiResponse(val status: Status, val result: Any?=null, val error: String?="")
