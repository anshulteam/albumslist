package com.android.kotlintest.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import java.text.SimpleDateFormat
import java.util.*

class Utils {

    companion object{

       fun maskString(strText: String?, start: Int, end: Int, maskChar: Char): String? {
            var start = start
            var end = end
            if (strText == null || strText == "") return ""
           if (start != null) {
               if (start < 0) start = 0
           }
           if (end != null) {
               if (end > strText.length) end = strText.length
           }
            val maskLength = end?.minus(start)
            if (maskLength == 0) return strText
            val sbMaskString = StringBuilder(maskLength)
            for (i in 0 until maskLength) {
                sbMaskString.append(maskChar)
            }
            return (strText.substring(0, start)
                    + sbMaskString.toString()
                    + strText.substring(start + maskLength))
        }

        fun convertUTCDateToLocalTime(utcTime: String?): String? {
            var time = ""
            if (utcTime != null) {
                val utcFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
                utcFormatter.setTimeZone(TimeZone.getTimeZone("UTC"))
                var gpsUTCDate = utcFormatter.parse(utcTime)
                val localFormatter = SimpleDateFormat("dd-MMM-yyyy HH:mm a", Locale.ENGLISH)
                time = localFormatter.format(gpsUTCDate?.getTime())
            }
            return time
        }

        fun isNetworkAvailable(context: Context?): Boolean {
            if (context == null) return false
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    when {
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                            return true
                        }
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                            return true
                        }
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                            return true
                        }
                    }
                }
            } else {
                val activeNetworkInfo = connectivityManager.activeNetworkInfo
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                    return true
                }
            }
            return false
        }
    }
}